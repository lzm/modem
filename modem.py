import requests
import datetime
import json

import info
import dsl
import log

auth = ('admin', 'gvt12345')
base = 'http://192.168.1.1/'
datestr = datetime.datetime.utcnow().isoformat()

pages = [
	('info', 'info.html', info.process),
	('dsl', 'statsadsl.html', dsl.process),
	('log', 'logview.cmd', log.process)
]

def process(tag, url, func):
	html = requests.get(base + url, auth=auth).text
	open('raw/' + tag + '-' + datestr + '.txt', 'w').write(html)
	return tag, func(html)

def get_data():
	data = dict(process(*page) for page in pages)
	data['date'] = datestr
	return data

if __name__ == '__main__':
	json.dump(get_data(), open('json/' + datestr + '.txt', 'w'))
