import sys
import json
import os

def diff(oldlog, newlog):
	m = min(len(oldlog), len(newlog))

	for i in range(m, 0, -1):
		if oldlog[-i:] == newlog[:i]:
			return newlog[i:]

	return newlog

def join(logs):
	acc, old = [], []

	for log in logs:
		acc.extend(diff(old, log))
		old = log

	return acc

if __name__ == '__main__':
	files = sorted(os.listdir('json'))
	logs = [json.load(open('json/' + f, 'r'))["log"] for f in files]
