import sys
import re

def process(html):
	cells = re.findall(r'<td[^>]*>([^<]+)</td>', html)
	cells = [c.strip() for c in cells if 'Gateway' not in c]
	return zip(*[cells[i::2] for i in range(2)])

if __name__ == '__main__':
	html = sys.stdin.read()
	data = process(html)

	for line in data:
		print line
