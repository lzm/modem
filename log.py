import sys
import re

def process_line(date, facility, severity, message):
	return (date, facility, severity, message.strip())

def process(html):
	cells = re.findall(r'<td>([^<]+)</td>', html)
	lines = zip(*[cells[i::4] for i in range(4)])
	return [process_line(*line) for line in lines]

if __name__ == '__main__':
	html = sys.stdin.read()
	data = process(html)

	for line in data:
		print line
