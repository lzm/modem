import sys
import re

def parse(cell):
	value = cell.strip()
	try:
		value = float(cell)
		value = int(cell)
	except:
		pass
	return value

def process(html):
	cells = re.findall(r'<(?:td|nobreak)[^>]*>([^<&]+)</(?:td|nobreak)>', html)
	data = []

	for cell in cells:
		if ':' in cell:
			cur = []
			data.append(cur)
		cur.append(parse(cell))

	return data

if __name__ == '__main__':
	html = sys.stdin.read()
	data = process(html)

	for line in data:
		print line
